# Tornillo-Bot

## Introduction

Tornillo-Bot is a Telegram bot designed to perform a variety of tasks, including interacting with external APIs and providing information based on user commands. It's a versatile tool that can be customized for various purposes, such as weather updates, news, or other city-specific information.

## Features

- Interaction with Telegram API.
- Customizable for various functionalities.
- Integration with external APIs.
- Supports multiple cities for location-based information.

## Getting Started

### Prerequisites

- Docker and Docker Compose installed.
- Telegram account and a bot token from the [BotFather](https://t.me/botfather).

### Installation

1. Clone the repository:
   \```bash
   git clone https://gitlab.com/x11tete11x/tornillo-bot.git
   cd tornillo-bot
   \```

2. Create a `.env` file at the root of the project and populate it with your settings:
   \```env
   BOT_TOKEN=your_unique_bot_token
   API_KEY=your_api_key_for_external_services
   DATABASE_URL=postgresql://username:password@localhost/dbname
   LOG_LEVEL=info
   TELEGRAM_TOKEN=your_telegram_bot_token
   TELEGRAM_CHAT_ID=your_telegram_chat_id
   API_ENDPOINT=your_api_endpoint
   CITIES_TO_CHECK=city1,city2,city3
   \```

3. Build and run using Docker:
   \```bash
   docker-compose up --build
   \```

## Configuration

To configure Tornillo-Bot for optimal performance and functionality, follow these steps:

1. **Setting up the `.env` File**: 
   - Navigate to the root directory of the project.
   - Create a file named `.env` if it does not already exist.
   - Add the necessary environment variables. Here's a brief explanation of each:
     - `BOT_TOKEN`: Your bot's unique token for authentication.
     - `API_KEY`: API key for any external services the bot interacts with.
     - `DATABASE_URL`: Connection string for the database.
     - `LOG_LEVEL`: The level of logging detail (e.g., `info`, `debug`).
     - `TELEGRAM_TOKEN`: Token provided by Telegram for bot integration.
     - `TELEGRAM_CHAT_ID`: The ID of the Telegram chat where the bot will operate.
     - `API_ENDPOINT`: Endpoint URL for external APIs the bot uses.
     - `CITIES_TO_CHECK`: List of cities the bot will monitor or provide information for, separated by commas.

2. **Configuring Telegram**:
   - If you haven't already, create a new bot on Telegram using [BotFather](https://t.me/botfather).
   - After creating your bot, BotFather will provide a token; this is your `TELEGRAM_TOKEN`.
   - Determine the chat ID (`TELEGRAM_CHAT_ID`) where the bot will send messages. This could be your personal chat ID or a group chat ID.

3. **Database Setup**:
   - Ensure that the database server is running and accessible.
   - Update the `DATABASE_URL` in your `.env` file with the correct credentials and database details.

4. **External API Setup**:
   - If the bot uses any external APIs, ensure that you have valid API keys.
   - Update the `API_KEY` and `API_ENDPOINT` variables in your `.env` file accordingly.

5. **Additional Configuration**:
   - Modify any additional settings or configurations specific to your bot’s functionality.
   - This may include setting up webhook URLs, configuring bot commands, or other bot-specific settings.

After completing these steps, your Tornillo-Bot should be configured and ready for deployment. Make sure to test the bot in a controlled environment before making it live to ensure all configurations are correct and functioning as expected.


## Usage

Provide instructions on how to use the bot, including any commands that can be used, how to interact with it on Telegram, and any other relevant usage information.

## Contributing

Contributions to the "tornillo-bot" are welcome. Please ensure you follow the coding standards and submit merge requests for any changes.