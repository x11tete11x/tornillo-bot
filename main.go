package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	telegramToken   = os.Getenv("TELEGRAM_TOKEN")
	telegramChatID  = os.Getenv("TELEGRAM_CHAT_ID")
	cities_to_check = getEnvOrDefaultCities("CITIES_TO_CHECK", []string{"bahia_blanca"})
	telegramAPI     = getEnvOrDefault("TELEGRAM_API", "https://api.telegram.org/bot")
	apiEndpoint     = os.Getenv("API_ENDPOINT") // Base URL of your API
	updateInterval  = getEnvAsDuration("UPDATE_INTERVAL", 10*time.Second)
	checkInterval   = getEnvAsDuration("CHECK_INTERVAL", 1*time.Hour)
	compare_temp   = getEnvOrDefaultInt("COMPARE_TEMP", 24)
)

var lastUpdateID int

type TelegramUpdate struct {
	UpdateID int `json:"update_id"`
	Message  struct {
		Text string `json:"text"`
		Chat struct {
			ID int `json:"id"`
		} `json:"chat"`
	} `json:"message"`
}

type TelegramResponse struct {
	Result []TelegramUpdate `json:"result"`
}

func getUpdates(offset int) ([]TelegramUpdate, error) {
	url := fmt.Sprintf("%s%s/getUpdates?offset=%d", telegramAPI, telegramToken, offset)
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var telegramResponse TelegramResponse
	err = json.Unmarshal(body, &telegramResponse)
	return telegramResponse.Result, err
}

func sendTelegramMessage(chatID int, message string) error {
	fmt.Println("MENSAJE: "+message)
	url := fmt.Sprintf("%s%s/sendMessage", telegramAPI, telegramToken)
	data := fmt.Sprintf(`{"chat_id": "%d", "text": "%s"}`, chatID, message)
	req, err := http.NewRequest("POST", url, bytes.NewBufferString(data))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	return nil
}

func containsIgnoreCase(s, substr string) bool {
    return strings.Contains(strings.ToLower(s), strings.ToLower(substr))
}

func printMapAsJSON(m map[string]interface{}) {
    jsonBytes, err := json.MarshalIndent(m, "", "  ")
    if err != nil {
        fmt.Println("Error marshaling map to JSON: %v", err)
    }
    fmt.Println(string(jsonBytes))
}

func msg_command(command,city string,data map[string]interface{}) (string){
	switch command {
	case "/city_wind":
		return fmt.Sprintf("🍃 Wind for %s: %s Km/h", city, data["wind"])
	case "/city_temperature":
		return fmt.Sprintf("🌡️ Temperature for %s: %s °", city, data["temperature"])
	case "/city_status":
		return fmt.Sprintf("📺 Status for %s: %s", city, data["status"])
	case "/city_humidity":
		return fmt.Sprintf("🚰 Humidity for %s: %s %%", city, data["humidity"])
	case "/city_pressure":
		return fmt.Sprintf("🍲 Pressure for %s: %s hPa", city, data["pressure"])
	case "/thermal_sensation":
		return fmt.Sprintf("🥵 Thermal Sensation for %s: %s hPa", city, data["thermal_sensation"])
	case "/city":
		return fmt.Sprintf("%s\n\n%s\n\n%s\n\n%s\n\n%s\n\n%s",msg_command("/city_wind",city,data),msg_command("/city_temperature",city,data),msg_command("/city_status",city,data),msg_command("/city_humidity",city,data),msg_command("/city_pressure",city,data),msg_command("/thermal_sensation",city,data))
	default:
		return ""
	}
}

func build_api_method(command string,args []string) (string,string,error){
	if len(args) == 0 {
		return "","",fmt.Errorf("Usage: %s [city] [timestamp - optional]",command)
	}
	timestamp := "true" // Default value if timestamp is not provided
	if len(args) > 1 {
		if args[1] == "true" || args[1] == "timestamp" {
			timestamp = "true"
		} else {
			timestamp = "false"
		}
	}
	switch command {
	case "city":
		return args[0],fmt.Sprintf("%s?city=%s",command, args[0]),nil
	default:
		return args[0],fmt.Sprintf("%s?city=%s&timestamp=%s",command, args[0], timestamp),nil
	}
}

func append_timestamp(msg string,data map[string]interface{}) (string) {
	val,ok:= data["data_timestamp"]
	if ok {
		return fmt.Sprintf("%s\n\n⌚ Data Timestamp: %s",msg,val)
	} else {
		return msg
	}
}

func handleCommand(chatID int, messageText string) {
	parts := strings.Fields(messageText)
	if len(parts) == 0 {
		sendTelegramMessage(chatID, "No command provided.")
		return
	}

	command := parts[0]
	args := parts[1:]

	switch command {
	case "/city_wind","/city_temperature","city_status","city_humidity","city_pressure","/city":
		city,apiMethod,err := build_api_method(command,args)
		if err != nil {
			sendTelegramMessage(chatID, err.Error())
			return
		}
		apiData, err := callYourAPI(apiMethod)
		if err != nil {
			sendTelegramMessage(chatID, fmt.Sprintf("Error: %s", err.Error()))
			return
		}
		printMapAsJSON(apiData)
		
		msg := msg_command(command,city,apiData)
		msg = append_timestamp(msg,apiData)
		sendTelegramMessage(chatID, msg)
	case "/chatid":
		chatIDMessage := fmt.Sprintf("Your Chat ID is: %d", chatID)
		sendTelegramMessage(chatID, chatIDMessage)
	default:
		sendTelegramMessage(chatID, "Unknown command.")
	}
}

func callYourAPI(apiMethod string) (map[string]interface{}, error) {
	url := apiEndpoint + apiMethod
	resp, err := http.Get(url)

	// Declare a map to hold the JSON data
    var responseJSON map[string]interface{}

	if err != nil {
		return make(map[string]interface{}), err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return make(map[string]interface{}), err
	}

	// Unmarshal the JSON string into the map
	err = json.Unmarshal([]byte(string(body)), &responseJSON)
	if err != nil {
		return nil, err // Return nil and the error
	}

	return responseJSON, nil
}

func checkOpenWindows() {
    for {
        for _, city := range cities_to_check {
            apiMethod := fmt.Sprintf("/city?city=%s", city)
            apiData, err := callYourAPI(apiMethod)
            if err != nil {
                fmt.Printf("Error checking wind direction for %s: %v\n", city, err)
                continue
            }

            wind, ok := apiData["wind"].(string) // Type assertion
            if !ok {
                fmt.Printf("Error: wind data for %s is not a string\n", city)
                continue
            }
			fmt.Println(wind)

			temperature, ok := apiData["temperature"].(string) // Type assertion
            if !ok {
                fmt.Printf("Error: temperature data for %s is not a string\n", city)
                continue
            }

            if containsIgnoreCase(wind, "Sur") || containsIgnoreCase(wind, "Sudoeste") || containsIgnoreCase(wind, "Sudeste") || compare_temp < 26 {
                telegramChatIDInt, err := strconv.Atoi(telegramChatID)
                if err != nil {
                    fmt.Printf("Invalid TELEGRAM_CHAT_ID: %v\n", err)
                    continue
                }

				if compare_temp < 26{
					sendTelegramMessage(telegramChatIDInt, fmt.Sprintf("🚨 ALERTA 🚨: BAJO la temperatura en %s! 🥰\n\nViento: %s Km/h\nTemperatura: %s°\n\n❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️\n❄️ ABRI TODAS LAS VENTANAS ❄️\n❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️", city, wind,temperature))
				} else {
					sendTelegramMessage(telegramChatIDInt, fmt.Sprintf("🚨 ALERTA 🚨: El viento se puso del SUR en %s! 🥰\n\nViento: %s Km/h\nTemperatura: %s°\n\n❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️\n❄️ ABRI TODAS LAS VENTANAS ❄️\n❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️", city, wind,temperature))
				}
                
            }
        }
        time.Sleep(checkInterval)
    }
}


func handleUpdates() {
	for {
		updates, err := getUpdates(lastUpdateID + 1)
		if err != nil {
			fmt.Println("Error in getting updates:", err.Error())
			time.Sleep(updateInterval)
			continue
		}

		for _, update := range updates {
			lastUpdateID = update.UpdateID
			go handleCommand(update.Message.Chat.ID, update.Message.Text)
		}

		time.Sleep(updateInterval)
	}
}

func getEnvOrDefault(envName string, defaultVal string) string {
	val := os.Getenv(envName)
	if val == "" {
		return defaultVal
	}
	return val
}

func getEnvOrDefaultInt(envName string, defaultVal int) int {
	val := os.Getenv(envName)
	if val == "" {
		return defaultVal
	}

	toRet,err := strconv.Atoi(val)
	if err != nil{
		return defaultVal
	} else {
		return toRet
	}
}

func getEnvOrDefaultCities(envName string, defaultVal []string) []string {
	val := os.Getenv(envName)
	if val == "" {
		return defaultVal
	}
	return strings.Split(val, ",")
}

func getEnvAsDuration(envName string, defaultVal time.Duration) time.Duration {
	valStr := os.Getenv(envName)
	if valStr == "" {
		return defaultVal
	}

	valInt, err := strconv.ParseInt(valStr, 10, 64)
	if err != nil {
		fmt.Printf("Error parsing %s: %v. Using default value.\n", envName, err)
		return defaultVal
	}

	return time.Duration(valInt) * time.Second
}

func main() {
	go checkOpenWindows() // Run wind check in a separate goroutine
	handleUpdates()         // Handle Telegram updates in the main goroutine
}
